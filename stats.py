# Statistique sur le fichier

location = './data/'

file = '3A-CPE-2020-2023.csv'

stream = open(location+file, "r")

lines = stream.readlines()

hommes = 0
femmes = 0

fillieres = {}

fournisseurs = {}

homonymes_nom = {}
homonymes_prenom = {}

parrains = {
    "CGP": {},
    "GPI": {},
    "ICS": {},
    "IRC": {},
    "SN": {},
}

for line in lines:
    line = line.rstrip('\n')
    array = line.split(',')
    if array[0] == 'MR':
        hommes += 1
    else:
        femmes += 1

    if array[5] in fillieres:
        fillieres[array[5]] += 1
    else:
        fillieres[array[5]] = 1

    if array[4] in parrains[array[5]]:
        parrains[array[5]][array[4]] += 1
    else:
        parrains[array[5]][array[4]] = 1



    if array[3] and (array[3].split('@')[1] in fournisseurs):
        fournisseurs[array[3].split('@')[1]] += 1
    elif array[3]:
        fournisseurs[array[3].split('@')[1]] = 1

    if array[1] in homonymes_nom:
        homonymes_nom[array[1]] += 1
    else:
        homonymes_nom[array[1]] = 1

    print(array)

    if array[2] not in homonymes_prenom:
        homonymes_prenom[array[2]] = {}

    homonymes_prenom[array[2]] = {
        "count" : (homonymes_prenom[array[2]]['count'] + 1 if 'count' in homonymes_prenom[array[2]] else 1),
        "MR": (homonymes_prenom[array[2]]['MR'] + (1 if array[0] == 'MR' else 0) if 'MR' in homonymes_prenom[array[2]] else (1 if array[0] == 'MR' else 0)),
        "MME": (homonymes_prenom[array[2]]['MME'] + (1 if array[0] == 'MME' else 0) if 'MME' in homonymes_prenom[array[2]] else (1 if array[0] == 'MME' else 0))
    }




print(hommes, femmes, (femmes / (hommes + femmes)) * 100, hommes+femmes)

print(fillieres)

print(fournisseurs)

print({k: v for k, v in sorted(fournisseurs.items(), key=lambda item: item[1])})

print(homonymes_prenom)

print(parrains['IRC'])

for filliere in parrains:
    i = 0
    for parrain in parrains[filliere]:
        if parrains[filliere][parrain] >= 3:
            i += 1
            print(filliere, parrain, parrains[filliere][parrain])
    print('Parrains avec plus d\'un fillot en',filliere, ' : ', i)
i = False
for prenom in homonymes_prenom.keys():
    if homonymes_prenom[prenom]['MR'] > 0 and homonymes_prenom[prenom]['MME'] > 0:
        i = True
        print(prenom)

if i == False:
    print("Aucun homme ne partage le même prénom qu'une femmme.")
#     if homonymes_prenom[prenom] > 1:
#         print(prenom, homonymes_prenom[prenom])
#
# for nom in homonymes_nom.keys():
#     if homonymes_nom[nom] > 1:
#         print(nom, homonymes_nom[nom])



stream.close()

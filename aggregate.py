#Aggregrer les différents fichiers

location = './data/'

total = location+'3A-CPE-2020-2023.csv'

files = [
    ['3CGP.csv','CGP'],
    ['3GPI.csv','GPI'],
    ['3ICS.csv','ICS'],
    ['3IRC.csv','IRC'],
    ['3SN.csv','SN']
]

write_stream = open(total, 'a+')


for file in files:
    stream = open(location+file[0], "r")
    lines = stream.readlines()
    for line in lines:
        line = line.rstrip('\n')
        array = line.split(',')
        if len(array) > 5:
            array_tmp = []
            i = 0
            for doc in array:
                if i < 5:
                    array_tmp.append(doc)
                i += 1
            array = array_tmp
        write_stream.write(','.join(array)+(",%s\n") % file[1])

    stream.close()

write_stream.close()
